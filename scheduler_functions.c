/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "scheduler_functions.h"
#include "../../MCAL_drivers/Timer_driver/Timer_functions.h"
#include "../../MCAL_drivers/INT_driver/INT_functions.h"


// --- internal --- //
#define SCHD_TIME_BASE 100 // scheduler time base in us

static u8 isInitialised = 1; // used to pause/resume the scheduler safely

typedef struct
{
    SCHD_CB_t SCHD_CB_func; // callback function
    u16 taskPeriod;         // repeated how many unit times
    u16 taskFirstDelay;     // unit times to skip at startup
    u8 isTaskPaused;        // continuity state (0 -> resumed, 1 -> paused)
} SCHD_task_t;

#define SCHD_TASK_LENGTH 5
static SCHD_task_t SCHD_task_Queue[SCHD_TASK_LENGTH]; // tasks array

// Scheduler callback (called each 1 uSec)
static void SCHD_vidExec(void)
{
    for (u8 i = 0; i < SCHD_TASK_LENGTH; i++) // foreach task
    {
        // if callback is empty (deregistered)
        if (!SCHD_task_Queue[i].SCHD_CB_func)
            continue;

        if (SCHD_task_Queue[i].taskFirstDelay) // task is still delayed
            SCHD_task_Queue[i].taskFirstDelay--;
        else                                   // task is allowed to run
        {
            // if task is not paused, then execute it
            if (!SCHD_task_Queue[i].isTaskPaused)
                SCHD_task_Queue[i].SCHD_CB_func();

            SCHD_task_Queue[i].taskFirstDelay = SCHD_task_Queue[i].taskPeriod - 1;
        }
    }
}

// unsafe version
static inline void SCHD_vidSetInitDelay_(const u8 u8PositionCpy, u16 u16DelayCpy, SCHD_time_t enumTimeCpy)
{
    switch(enumTimeCpy)
    {
        case SCHD_time_mS:
            u16DelayCpy  *= ((u16)1000/SCHD_TIME_BASE);
        break;

        case SCHD_time_uS:
            u16DelayCpy  /= SCHD_TIME_BASE;
        break;
    }

    SCHD_task_Queue[u8PositionCpy].taskFirstDelay = u16DelayCpy;
}

// unsafe version
static inline void SCHD_vidSetPeriod_(const u8 u8PositionCpy, u16 u16PeriodCpy, SCHD_time_t enumTimeCpy)
{
    if (u16PeriodCpy) // period exists
    {
        switch(enumTimeCpy)
        {
            case SCHD_time_mS:
                u16PeriodCpy *= ((u16)1000/SCHD_TIME_BASE);
            break;

            case SCHD_time_uS:
                u16PeriodCpy /= SCHD_TIME_BASE;
            break;
        }

        SCHD_task_Queue[u8PositionCpy].taskPeriod = u16PeriodCpy;
    }
    else              // period doesn't exist (paused function)
    {
        SCHD_task_Queue[u8PositionCpy].taskPeriod = 1;
        SCHD_task_Queue[u8PositionCpy].isTaskPaused = 1;
    }

}
// ---------------- //

void SCHD_vidInit(void)
{
    for (u8 i = 0; i < SCHD_TASK_LENGTH; i++)
    {
        SCHD_task_Queue[i].SCHD_CB_func = 0;
        SCHD_task_Queue[i].isTaskPaused = 1;

        SCHD_task_Queue[i].taskPeriod = 0;
        SCHD_task_Queue[i].taskFirstDelay = 0;
    }

    // scheduler state is un-paused (resumed)
    isInitialised = 0;

    // setup timer2 parameters
    Timer_vidInit( Timer2_prescaler_off,   // timer should start as turned off
                   Timer_mode_CTC,         // timer mode: CTC (compare-match)
                   2                       // timer select: 2
                 );

    // timer2 compare value
    Timer_vidSetCmpValue(199, 2);

    // register timer2 compare-match callback
    Timer_vidRegisterCB_Cmp(SCHD_vidExec, 2);

    // enable timer2 compare-match INT
    Timer_vidEnableCmpINT(2);

    // enable global INT flag
    INT_vidEnable_global_flag();

    // turn on timer2
    Timer_vidSetClock(Timer2_prescaler_div_8, 2);
}

void SCHD_vidResumeSCHD(void)
{
    if (!isInitialised)
        return;

    // register timer2 compare-match callback
    Timer_vidRegisterCB_Cmp(SCHD_vidExec, 2);
}

void SCHD_vidPauseSCHD(void)
{
    if (!isInitialised)
        return;

    // deregister timer2 compare-match callback
    Timer_vidDeregisterCB_Cmp(2);
}

void SCHD_vidRegisterCB(SCHD_CB_t CBfuncCpy, u16 u16PeriodCpy, u16 u16DelayCpy, SCHD_time_t enumTimeCpy, const u8 u8PositionCpy)
{
    if (u8PositionCpy >= SCHD_TASK_LENGTH)
        return;

    SCHD_vidSetInitDelay_(u8PositionCpy, u16DelayCpy, enumTimeCpy);
    SCHD_vidSetPeriod_(u8PositionCpy, u16PeriodCpy, enumTimeCpy);

    SCHD_task_Queue[u8PositionCpy].isTaskPaused = 1;
    SCHD_task_Queue[u8PositionCpy].SCHD_CB_func = CBfuncCpy;
}

void SCHD_vidDeregisterCB(const u8 u8PositionCpy)
{
    if (u8PositionCpy >= SCHD_TASK_LENGTH)
        return;

    SCHD_task_Queue[u8PositionCpy].SCHD_CB_func = 0;
    SCHD_task_Queue[u8PositionCpy].isTaskPaused = 1;

    SCHD_task_Queue[u8PositionCpy].taskPeriod = 0;
    SCHD_task_Queue[u8PositionCpy].taskFirstDelay = 0;
}

inline void SCHD_vidResumeTask(const u8 u8PositionCpy)
{
    if (u8PositionCpy >= SCHD_TASK_LENGTH)
        return;

    if (!SCHD_task_Queue[u8PositionCpy].SCHD_CB_func)
        return;

    SCHD_task_Queue[u8PositionCpy].isTaskPaused = 0;
}

inline void SCHD_vidPauseTask(const u8 u8PositionCpy)
{
    if (u8PositionCpy >= SCHD_TASK_LENGTH)
        return;

    if (!SCHD_task_Queue[u8PositionCpy].SCHD_CB_func)
        return;

    SCHD_task_Queue[u8PositionCpy].isTaskPaused = 1;
}

void SCHD_vidSetInitDelay(const u8 u8PositionCpy, u16 u16DelayCpy, SCHD_time_t enumTimeCpy)
{
    if (u8PositionCpy >= SCHD_TASK_LENGTH)
        return;

    if (!SCHD_task_Queue[u8PositionCpy].SCHD_CB_func)
        return;

    SCHD_vidSetInitDelay_(u8PositionCpy, u16DelayCpy, enumTimeCpy);
}

void SCHD_vidSetPeriod(const u8 u8PositionCpy, u16 u16PeriodCpy, SCHD_time_t enumTimeCpy)
{
    if (u8PositionCpy >= SCHD_TASK_LENGTH)
        return;

    if (!SCHD_task_Queue[u8PositionCpy].SCHD_CB_func)
        return;

    SCHD_vidSetPeriod_(u8PositionCpy, u16PeriodCpy, enumTimeCpy);
}

