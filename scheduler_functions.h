/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef SERVICES_SCHD_FUNCTIONS_SCHD_HELPER_FUNCTIONS_H_
#define SERVICES_SCHD_FUNCTIONS_SCHD_HELPER_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

// time base is 100 us

typedef enum
{
    SCHD_time_uS,
    SCHD_time_mS
} SCHD_time_t;

typedef void (*SCHD_CB_t)(void);

void SCHD_vidInit(void);

void SCHD_vidResumeSCHD(void);
void SCHD_vidPauseSCHD(void);

void SCHD_vidRegisterCB(SCHD_CB_t CBfuncCpy, u16 u16PeriodCpy, u16 u16DelayCpy, SCHD_time_t enumTimeCpy, const u8 u8PositionCpy);
void SCHD_vidDeregisterCB(const u8 u8PositionCpy);

void SCHD_vidResumeTask(const u8 u8PositionCpy);
void SCHD_vidPauseTask(const u8 u8PositionCpy);

void SCHD_vidSetInitDelay(const u8 u8PositionCpy, u16 u16DelayCpy, SCHD_time_t enumTimeCpy);
void SCHD_vidSetPeriod(const u8 u8PositionCpy, u16 u16PeriodCpy, SCHD_time_t enumTimeCpy);


#endif /* SERVICES_SCHD_FUNCTIONS_SCHD_HELPER_FUNCTIONS_H_ */

